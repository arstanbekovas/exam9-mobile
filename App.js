import React from 'react';
import {createStore, applyMiddleware} from "redux";
import reducer from "./src/store/reducer";
import ContactsList from "./src/container/ContactsList";
import Provider from "react-redux/es/components/Provider";
import thunk from "redux-thunk";

const store = createStore(reducer, applyMiddleware(thunk));



export default class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <ContactsList/>
            </Provider>
        );
    }
}


