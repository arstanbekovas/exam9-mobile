import React, {Component} from 'react';
import {Button, FlatList, Modal, StyleSheet, Text, TouchableHighlight, View} from 'react-native';
import {connect} from 'react-redux';
import ContactControl from "../components/ContactControl";
import { hideModal, showContacts, showModal} from "../store/actions";


class ContactsList extends Component {
    state = {
        contacts: {}
    };

    componentDidMount() {
        this.props.onShowContacts();
    }

    contactPressedHandler = () => {

        alert('Place was pressed!');
    };

    render() {
        console.log('contacts',this.props.contacts)
        return (
            <View style={styles.container}>
                <Text style={styles.largeText}>Contacts</Text>
                <FlatList
                    style={styles.itemsList}
                    data={Object.values(this.props.contacts)}
                    renderItem={(item) => {
                        console.log(item.item);
                        return(
                        <ContactControl
                            style={styles.itemsList}
                            pic={item.item.url}
                            name={item.item.name}
                            contactPressed={this.contactPressedHandler} />
                    )}}
                    />
                {/*<Modal*/}
                    {/*animationType="slide"*/}
                    {/*transparent={false}*/}
                    {/*visible={this.props.showModal}*/}
                    {/*onRequestClose={() => {*/}
                        {/*alert('Modal has been closed.');*/}
                    {/*}}>*/}
                    {/*<View style={{marginTop: 22}}>*/}
                        {/*<View>*/}
                            {/*<Text>Hello World!</Text>*/}
                            {/*<TouchableHighlight*/}
                                {/*onPress={() => {*/}
                                    {/*this.props.onHideModal();*/}
                                {/*}}>*/}
                                {/*<Text>Hide Modal</Text>*/}
                            {/*</TouchableHighlight>*/}
                        {/*</View>*/}
                    {/*</View>*/}
                {/*</Modal>*/}

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'flex-start',
        padding: 30
    },
    input: {
        width: '80%',
        height: 40
    },
    largeText: {
        fontSize: 20,
        padding: 10
    },
    inputContainer: {
        width: '100%',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'flex-start'
    },
    itemsList: {
        width: '100%',
    }
});

const mapStateToProps = state => {
    return {
        contacts: state.contacts,
        showModal: state.showModal
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onShowContacts: () => dispatch(showContacts()),
        onShowModal: () => dispatch(showModal()),
        onHideModal: () => dispatch(hideModal())

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(ContactsList);