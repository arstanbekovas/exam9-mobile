import React from 'react';
import {Text, View, StyleSheet, TouchableOpacity, Image} from "react-native";

const ContactControl = props => (
    <TouchableOpacity onPress={props.contactPressed}>
        <View style={styles.contactControl}>
            <Image
                style={{width: 60, height: 50}}
                source={{uri: props.pic}}
            />
            <Text style={styles.name}>{props.name}</Text>
        </View>
    </TouchableOpacity>
);

const styles = StyleSheet.create({
    contactControl: {
        width: '100%',
        backgroundColor: '#eee',
        marginBottom: 10,
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        fontSize: 17
    },
    name: {
        fontWeight: 'bold'
    }
});

export default ContactControl;