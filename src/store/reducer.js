import * as actionTypes from "./actionTypes";


const initialState = {
    contacts: {},
    selectedContact: {},
    showModal: false,
};

const reducer = (state=initialState, action) => {
    switch (action.type) {
        case actionTypes.CONTACT_SUCCESS:
            console.log(action.info );
            return {...state, contacts: action.info};

        case actionTypes.SHOW_MODAL:
            return {...state, showModal: true};
        case actionTypes.HIDE_MODAL:
            return {...state, showModal: false};
        default:
            return state;
    }

};

export default reducer;