import * as actionTypes from './actionTypes';
import axios from "../../axios-contact";


export const contactRequest = () => {
    return {type: actionTypes.CONTACT_REQUEST};
};
export const contactSuccess = (info) => {
    return {type: actionTypes.CONTACT_SUCCESS, info};
};

export const contactError = (error) => {
    return {type: actionTypes.CONTACT_ERROR, error};
};

export const showModal = () => {
    console.log('modal');
    return {type: actionTypes.SHOW_MODAL};
};
export const hideModal = () => {
    return {type: actionTypes.HIDE_MODAL};
};

export const showContacts = () => {
    return dispatch => {
        dispatch(contactRequest());
        axios.get('/contacts.json').then((response) => {
            console.log(response.data);
            dispatch(contactSuccess(response.data));
        }, error => {
            dispatch(contactError(error));
        });
    }
};




