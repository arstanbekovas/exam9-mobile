import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://exam9-contacts.firebaseio.com/'
});

export default instance;